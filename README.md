# indymilter

The indymilter project has moved to the [Codeberg] platform:

<https://codeberg.org/glts/indymilter>

Please update your links.

[Codeberg]: https://codeberg.org
